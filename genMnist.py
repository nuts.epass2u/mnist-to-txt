from tensorflow import keras
import numpy as np

from sys import argv
import random

random.seed(0)


(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

def genTXT(xx, yy, filename, count, shuffle=False, split=False):
    buf = {}
    for x,y in zip(xx,yy):
        if not y in buf:
            buf[y] = [x]
        else:
            buf[y].append(x)

    queue_y = []
    queue_x = []
    for y,x_buf in sorted(buf.items()):
        for x in x_buf[:count]:
            x = (x/128).clip(0,1).astype(np.int8)

            queue_y.append(str(y) + "\n")
                           
            entry = ""
            for row in x:
                entry += "".join(["#" if v else "." for v in row]) + "\n"

            queue_x.append(entry)

    queue = [*zip(queue_y, queue_x)]
    if shuffle:
        random.shuffle(queue)

    if not split:
        with open(filename+".txt", "w") as fp:
            for y,x in queue:
                fp.write(y+x)

    else:
        with open(filename+"_y.txt", "w") as fp_y:
            with open(filename+"_x.txt", "w") as fp_x:
                for y,x in queue:
                    fp_y.write(y)
                    fp_x.write(x)


if __name__ == "__main__":
    try:
        TRAIN_SIZE = int(argv[1])
    except:
        TRAIN_SIZE = 1000
    
    try:
        TEST_SIZE = int(argv[2])
    except:
        TEST_SIZE = 100

    genTXT(x_train, y_train, "train_sort", TRAIN_SIZE)
    genTXT(x_train, y_train, "train_shuffle", TRAIN_SIZE, shuffle=True)

    genTXT(x_test, y_test, "test_sort", TEST_SIZE, split=True)
    genTXT(x_test, y_test, "test_shuffle", TEST_SIZE, split=True, shuffle=True)
